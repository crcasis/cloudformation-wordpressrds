# cloudformation-wordpressrds

Deploy with 2 stacks one ec2 instance that install wordpress with php 7.2 and connect with rds instance (all automatizated) 

--
https://gitlab.com/crcasis/cloudformation-wordpressrds

Services: VPC: Private and Public Network, Nat gateways, Interface gateways, Elastic Ip, Ec2 Instance, rds mysql database, elastic file system (has to be linked with /var/www/html/wp-content), ACM, Route 53 and Cloudfront distribution.

Deploy stack bucket-vpc.yaml that create a bucket.
Deploy rds stack and edit vpc, subnets and security groups ids.


Edit:

Ec2-stack: userdata URLs, subnet ids and security group ids.

Initwordpress.sh → change environment variables

Letsencrypt.sh →change domain variables

Create a new database on rds instance and grant permissions → edit wp-config.php file



Upload in a new S3 bucket all this resources and edit ec2-vpc to take the resources from the bucket.
Example: 

            cd / sudo wget "https://$bucket/initwordpress.sh" & sudo sh initwordpress.sh   
            cd / & sudo wget "https://api.wordpress.org/secret-key/1.1/salt/" & sudo wget "https://$bucket/wp-config.php" & sudo mv -r wp-config.php /var/www/html/

Deploy ec2 stack (edit vpc, subnets and sg ids).
Mount efs file system /var/www/wp-config (optional point, after mount copy wp-content folder) 
Example:  sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 $endpointURL:/ /var/www/html/wp-content



Then create a new database in rds instance and connect to the database – https://gitlab.com/crcasis/cloudformation-wordpressrds/blob/master/files/createdbuserrds.sh
After deploy rds stack,  update in s3 bucket initwordpress.sh (dbhost will be different endpoint) and update the ec2 stack.
Example:

 

After update ec2 stack the file /var/www/html/wp-config.php has to be the new configuration.
Install ssl cert with let's encrypt – use letsencrypt.sh script and change the email configuration, domain and SSL certificate folder.
Deploy dns yaml with the new dns zone and records (add the elastic ip)
Deploy acm and cloudfront stack in Virginia zone and validate SSL Certificate by DNS Record (  ~5 min )