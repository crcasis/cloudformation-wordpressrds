#!/bin/bash
domainX="wordpress.ux.ebcont.com"
domainY="wordpress-ec2.ux.ebcont.com"
sudo amazon-linux-extras install epel -y
sudo yum install python-certbot-apache -y
sudo certbot certonly --webroot -w /var/www/html/ --renew-by-default --email cristian.casis@ebcont.com --text --agree-tos  -d $domainX -d $domainY
echo 'Listen 443 https
            <VirtualHost *:443>
            DocumentRoot "/var/www/html"
            SSLEngine on
            SSLCertificateFile /etc/letsencrypt/live/$domainX/cert.pem
            SSLCertificateKeyFile /etc/letsencrypt/live/$domainX/privkey.pem
            SSLCertificateChainFile /etc/letsencrypt/live/$domainX/fullchain.pem
            </VirtualHost>' > /etc/httpd/conf.d/ssl.conf & sudo systemctl restart httpd
sudo echo "sudo letsencrypt renew --dry-run --agree-tos" > /opt/renew-letsencrypt.sh & chmod +x /opt/renew-letsencrypt.sh
sudo echo "0 0 1 */6 * root sudo sh /opt/renew-letsencrypt.sh" >> /etc/crontab

