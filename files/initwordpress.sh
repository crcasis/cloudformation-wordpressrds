#!/bin/bash
sudo yum -y update
sudo yum install -y httpd mod_ssl
sudo amazon-linux-extras install epel -y
sudo amazon-linux-extras install php7.2 -y
sudo wget "http://wordpress.org/latest.tar.gz"
sudo tar xfz latest.tar.gz
sudo mv wordpress/* /var/www/html/
sudo chown -R apache /var/www
sudo chgrp -R apache /var/www
sudo chmod 2775 /var/www
sudo find /var/www -type d -exec sudo chmod 2775 {} \;
sudo find /var/www -type f -exec sudo chmod 0664 {} \;
sudo find /var/www -type f -exec sudo chmod 0664 {} \;
sudo systemctl enable httpd
sudo systemctl restart httpd
            